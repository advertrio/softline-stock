<? $title = 'Title' ?>
<? include 'partials/header.php' ?>
<div data-sticky-container>
    <nav class="navigation show-for-large" data-sticky data-margin-top="6.1">
        <div class="row">
            <div class="column small-12">
                <ul class="navigation__menu" data-magellan data-offset="120">
                    <li><a href="#about">О продукте</a></li>
                    <li><a href="#editions">Комплектация</a></li>
                    <li><a href="#">специальные</a></li>
                    <li><a href="#">предложения </a></li>
                    <li><a href="#">Подробное описание</a></li>
                    <li><a href="#">Мероприятия</a></li>
                </ul>
            </div>
        </div>
    </nav>
</div>
<main class="main -padding-y">
    <section class="product -margin-y" id="about" data-magellan-target="about">
        <div class="row">
            <div class="column small-12 medium-4">
                <div class="product__image">
                    <img src="dist/images/product/adobecc.png" alt="">
                </div>
            </div>
            <div class="column small-12 medium-8">
                <div class="product__text b-editor">
                    <h1>Adobe creative cloud</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                        laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
                        voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                    </p>
                    <p>
                        Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim
                        id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
                        doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi
                        architecto beatae vitae dicta sunt explicabo.
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section class="options-cards" id="editions" data-magellan-target="editions">
        <div class="row">
            <div class="column small-12">
                <div class="b-title -margin-y -large-offset">
                    <span>Комплектация</span>
                </div>
            </div>
        </div>
        <div class="row small-up-1 medium-up-2 large-up-3 options-cards__list">
            <div class="column">
                <div class="options-cards__item">
                    <div class="options-cards__item-top text-price">
                        <h4>Комплектация 1</h4>
                        <p><strong>10 000</strong><strong>₽</strong></p>
                        <p><span>12 500</span></p>
                        <div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
                                irure
                            </p>
                        </div>
                    </div>
                    <div class="options-cards__item-bottom">
                        <form action="">
                            <div class="count">
                                <button class="count__btn"><span class="add"></span></button>
                                <input value="2" type="text">
                                <button class="count__btn"><span class="remove"></span></button>
                            </div>
                            <button class="button">купить</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="column">
                <div class="options-cards__item">
                    <div class="options-cards__item-top text-price">
                        <h4>Комплектация 1</h4>
                        <p><strong>10 000</strong><strong>₽</strong></p>
                        <p><span>12 500</span></p>
                        <div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud

                            </p>
                        </div>
                    </div>
                    <div class="options-cards__item-bottom">
                        <form action="">
                            <div class="count">
                                <button class="count__btn"><span class="add"></span></button>
                                <input value="2" type="text">
                                <button class="count__btn"><span class="remove"></span></button>
                            </div>
                            <button class="button">купить</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="column">
                <div class="options-cards__item">
                    <div class="options-cards__item-top text-price">
                        <h4>Комплектация 1</h4>
                        <p><strong>10 000</strong><strong>₽</strong></p>
                        <p><span>12 500</span></p>
                        <div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua.
                            </p>
                        </div>
                    </div>
                    <div class="options-cards__item-bottom">
                        <form action="">
                            <div class="count">
                                <button class="count__btn"><span class="add"></span></button>
                                <input value="2" type="text">
                                <button class="count__btn"><span class="remove"></span></button>
                            </div>
                            <button class="button">купить</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="column">
                <div class="options-cards__item">
                    <div class="options-cards__item-top text-price">
                        <h4>Комплектация 1</h4>
                        <p><strong>10 000</strong><strong>₽</strong></p>
                        <p><span>12 500</span></p>
                        <div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua.
                            </p>
                        </div>
                    </div>
                    <div class="options-cards__item-bottom">
                        <form action="">
                            <div class="count">
                                <button class="count__btn"><span class="add"></span></button>
                                <input value="2" type="text">
                                <button class="count__btn"><span class="remove"></span></button>
                            </div>
                            <button class="button">купить</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="btn-more-wrapper"><a href="#" class="btn-more">показать еще</a></div>
    </section>
    <section class="offers">
        <div class="row">
            <div class="column small-12">
                <div class="b-title -margin-y -large-offset">
                    <span>Специальные предложения</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column small-12">
                <div class="offers__wrapper">
                    <div class="offers__card">
                        <div>
                            <p>Microsoft Azure <br class="show-for-large"> cloud</p>
                        </div>
                        <div class="flex-container flex-dir-column-reverse text-price">
                            <p class="lite"><span>10 000</span><span>₽</span></p>
                        </div>
                    </div>
                    <div class="offers__symbol"><span>+</span></div>
                    <div class="offers__card">
                        <div>
                            <p>Adobe Creative cloud</p>
                        </div>
                        <div class="flex-container flex-dir-column-reverse text-price">
                            <p class="lite"><span>10 000</span><span>₽</span></p>
                        </div>
                    </div>
                    <div class="offers__symbol"><span>+</span></div>
                    <div class="offers__card">
                        <div>
                            <p>Microsoft Office</p>
                        </div>
                        <div class="flex-container flex-dir-column-reverse text-price">
                            <p class="lite"><span>10 000</span><span>₽</span></p>
                        </div>
                    </div>
                    <div class="offers__symbol"><span>=</span></div>
                    <div class="offers__card -results">
                        <div class="text-price">
                            <p><strong>25 000</strong><strong>₽</strong></p>
                            <p><span>30 500</span><span>₽</span></p>
                            <a class="button" href="#">купить</a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <section style="padding-top: 40px">
        <div class="row">
            <div class="column small-12">
                <div class="b-title -margin-y -large-offset">
                    <span>Мероприятия</span>
                </div>
            </div>
        </div>
        <div class="row small-up-1 medium-up-2">
            <div class="column">
                <div class="events-card">
                    <div style="background-image: url(content/events-card/photo-1.jpg)" class="events-card__photo">
                    </div>
                    <div class="events-card__content">
                        <h6>Название <br content="show-for-large">
                            мероприятия</h6>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt
                            ut labore et dolore magna aliqua. Ut enim ad minim
                        </p>

                        <a href="#" class="button alternate">регистриция</a>
                    </div>
                </div>
            </div>
            <div class="column">
                <div class="events-card">
                    <div style="background-image: url(content/events-card/photo-2.jpg)" class="events-card__photo">
                    </div>
                    <div class="events-card__content">
                        <h6>Название
                            мероприятия</h6>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt
                            ut labore et dolore
                        </p>

                        <a href="#" class="button alternate">регистриция</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section style="padding-top: 40px">
        <div class="row">
            <div class="column small-12">
                <div class="b-title -margin-y -large-offset">
                    <span>Комплектация</span>
                </div>
            </div>
        </div>
        <div class="row align-center">
            <div class="column small-12">
                <span><img src="content/content-image-3.jpg" alt=""></span>
            </div>
            <div style="padding: 30px" class="column small-12 medium-11">
                <div class="b-editor -product">
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                        labore
                        et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
                        ut
                        aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. </p>
                    <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id
                        est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
                        doloremque
                        laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto
                        beatae vitae dicta sunt explicabo.
                    </p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="column small-12 medium-6 text-center">
                <span><img src="content/content-image-1.jpg" alt=""></span>
            </div>
            <div class="column small-12 medium-6">
                <div class="b-editor -product">
                    <h2>Title of the paragraph</h2>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                        laboris nisi ut aliquip ex ea commodo consequat.
                        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                        pariatur. perspiciatis unde omnis iste natus error.
                    </p>
                    <button data-open="pop-up-1" class="button alternate">pop-up кнопка</button>
                </div>
            </div>
        </div>

        <div style="padding-top: 30px" class="row">
            <div class="column small-12  medium-6">
                <div class="b-editor -product">
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                        laboris nisi ut aliquip ex ea commodo consequat.
                        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                        pariatur. perspiciatis unde omnis iste natus error.
                    </p>
                    <button class="button alternate">Типовая кнопка</button>
                </div>
            </div>
            <div class="column small-12 medium-6 text-center">
                <span><img src="content/content-image-2.jpg" alt=""></span>
            </div>
        </div>
    </section>
    <section class="video-container-wrap">
        <div class="row">
            <div class="column small-12">
                <div class="b-title -margin-y -large-offset">
                    <span>Видео продукта</span>
                </div>
            </div>
            <div class="column small-12">
                <div class="video-container">
                    <iframe width="100%" height="100%" src="https://www.youtube.com/embed/JgzYTkq7BQA" frameborder="0"
                            allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </section>
    <div class="pop-up reveal" id="pop-up-1" data-reveal>
        <div class="pop-up__title text-center">
            <span>Заголовок для формы</span>
        </div>
        <form action="">
            <div class="b-input">
                <div>
                    <span class="b-input__title">Ваше Ф.И.О</span>
                    <label>
                        <input placeholder="Введите ваше имя" type="text">
                    </label>
                </div>
            </div>
            <div class="b-input">
                <div>
                    <span class="b-input__title">Телефон</span>
                    <label>
                        <input placeholder="Введите номер телефона" type="text">
                    </label>
                </div>
            </div>
            <div class="b-input  -warning">
                <div>
                    <span class="b-input__title">Эл. почта</span>
                    <label>
                        <input value="ddd" placeholder="E-mail" type="text">
                    </label>
                </div>
            </div>
            <div class="b-input">
                <div>
                    <span class="b-input__title">Эл. почта</span>
                    <textarea name="" id="" cols="30" rows="10">

                    </textarea>
                </div>
            </div>
            <div class="b-input">
                <div>
                    <div class="abc-radio">
                        <input type="radio" name="radio1" id="radio1" value="option1" checked>
                        <label for="radio1">
                            oracle Check
                        </label>
                    </div>
                    <div class="abc-radio">
                        <input type="radio" name="radio1" id="radio2" value="option1" checked>
                        <label for="radio2">
                            oracle Check
                        </label>
                    </div>
                    <div class="abc-checkbox">
                        <input type="checkbox" id="checkbox-1" value="option1">
                        <label for="checkbox-1">
                            oracle Check radio
                        </label>
                    </div>

                    <div class="abc-checkbox">
                        <input type="checkbox" id="checkbox-2" value="option1" checked>
                        <label for="checkbox-2">
                            oracle Check radio
                        </label>
                    </div>
                </div>
            </div>
            <div class="b-input">
                <div>
                    <span class="b-input__title">Эл. почта</span>
                    <div>
                        <select>
                            <option data-display="Select" value="list1">Список номер два</option>
                            <option value="list2">Список выпадающей формы</option>
                            <option value="list3">Список формы</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <button class="button darker">отправить</button>
            </div>
        </form>
    </div>

</main>
<? include 'partials/footer.php' ?>
<script>
    $(document).foundation();
</script>
</body>
</html>