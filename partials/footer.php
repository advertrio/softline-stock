<footer class="footer -padding-top">
    <div class="row">
        <div class="column small-12">
            <div class="footer__top">
                <div class="footer__logo">
                    <a href="#"><img src="dist/images/footer__logo/footer-logo.png" alt=""></a>
                </div>

                <div class="footer__contacts">
                    <a href="#">
                    <span>
                       <svg class="icon icon-tel">
                           <use xlink:href="#icon-tel"></use>
                       </svg>
                    </span>
                        <span>+7 495 123 12 45</span>
                    </a>
                    <a href="#">
                        <span>
                            <svg class="icon icon-mail">
                           <use xlink:href="#icon-mail"></use>
                       </svg>
                        </span>
                        <span>services@softlinegroup.com</span>
                    </a>
                </div>

                <div class="footer__socials">
                    <a href="#">
                        <svg class="icon icon-fb">
                            <use xlink:href="#icon-fb"></use>
                        </svg>
                    </a>
                    <a href="#">
                        <svg class="icon icon-twitter">
                            <use xlink:href="#icon-twitter"></use>
                        </svg>
                    </a>
                    <a href="#">
                        <svg class="icon icon-vk">
                            <use xlink:href="#icon-vk"></use>
                        </svg>
                    </a>
                    <a href="#">
                        <svg class="icon icon-youtube">
                            <use xlink:href="#icon-youtube"></use>
                        </svg>
                    </a>
                    <a href="#">
                        <svg class="icon icon-habr">
                            <use xlink:href="#icon-habr"></use>
                        </svg>
                    </a>
                    <a href="#">
                        <svg class="icon icon-in">
                            <use xlink:href="#icon-in"></use>
                        </svg>
                    </a>
                    <a href="#">
                        <svg class="icon icon-telegram">
                            <use xlink:href="#icon-telegram"></use>
                        </svg>
                    </a>
                    <a href="#">
                        <svg class="icon icon-google-plus">
                            <use xlink:href="#icon-google-plus"></use>
                        </svg>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="footer__bottom">
        <div class="row">
            <div class="column small-12 large-6">
                <div class="flex-container">
                    <div class="footer__nav">
                        <ul>
                            <li><a href="#">о компании</a></li>
                            <li><a href="#">контакты</a></li>
                            <li><a href="#">новости</a></li>
                            <li><a href="#">мероприятия</a></li>
                        </ul>
                    </div>

                </div>
            </div>
            <div class="column small-12 large-6">
                <div class="flex-container">

                    <div class="footer__place">
                        <a href="#"> Москва, 119270, Лужнецкая набережная, д. 2/4, стр. 3А.</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<script src="/dist/javascript/bundle.js"></script>
<script>
    $(document).ready(function () {
        $('select').niceSelect();
    });
</script>