<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="dist/css/template_styles.css">
    <title><?= $title ?></title>
</head>
<body>
<div style="display: none">
    <?php include 'dist/images/icons/svg/symbols.svg' ?>
</div>

<header id="header">
    <div data-sticky-container>
        <div class="header sticky" data-sticky data-margin-top="0">
            <div class="row">
                <div class="column small-12">
                    <div class="header__top">
                        <div class="header__logo">
                            <a href="#"><img src="dist/images/softline-logo.png" alt=""></a>
                        </div>
                        <div class="header__contacts">
                            <a href="#">
                    <span>
                       <svg class="icon icon-tel">
                           <use xlink:href="#icon-tel"></use>
                       </svg>
                    </span>
                                <span>+7 495 123 12 45</span>
                            </a>
                            <a href="#">
                        <span>
                            <svg class="icon icon-mail">
                           <use xlink:href="#icon-mail"></use>
                       </svg>
                        </span>
                                <span>services@softlinegroup.com</span>
                            </a>
                        </div>
                        <div class="header__nav">
                            <button class="header__nav-btn hide-for-large" data-toggle="user-nav">
                                <svg class="icon icon-hamburger">
                                    <use xlink:href="#icon-hamburger"></use>
                                </svg>
                            </button>
                            <ul class="show-for-large">
                                <li><a href="#">о компании</a></li>
                                <li><a href="#">контакты</a></li>
                                <li><a href="#">новости</a></li>
                                <li><a href="#">мероприятия</a></li>
                            </ul>
                        </div>
                        <div class="profile">
                            <div class="profile__btn">
                                <button data-toggle="user-dropdown">
                            <span class="user-btn">
                                <svg class="icon icon-user">
                            <use xlink:href="#icon-user"></use>
                                </svg>
                            </span>
                                    <span>вы</span>
                                </button>
                            </div>

                            <div class="profile__pane dropdown-pane" id="user-nav" data-dropdown
                                 data-auto-focus="true">
                                <ul>
                                    <li><a href="#">о компании</a></li>
                                    <li><a href="#">контакты</a></li>
                                    <li><a href="#">новости</a></li>
                                    <li><a href="#">мероприятия</a></li>
                                </ul>
                            </div>

                            <div class="profile__pane dropdown-pane" id="user-dropdown" data-dropdown
                                 data-auto-focus="true">
                                <ul>
                                    <li><a href="#">Дмитрий Веровски Игоревич</a></li>
                                    <li>
                                        <hr>
                                    </li>
                                    <li><a href="#">Кабинет</a></li>
                                    <li><a href="#">Профиль</a></li>
                                    <li><a href="#">Выйти</a></li>
                                </ul>

                                <a class="profile__btn-exit" href="#">
                                    <svg class="icon icon-exit">
                                        <use xlink:href="#icon-exit"></use>
                                    </svg>
                                </a>
                            </div>

                            <div class="profile__btn">
                                <a href="#">
                            <span>
                                  <svg class="icon icon-basket">
                                <use xlink:href="#icon-basket"></use>
                            </svg>
                            </span>
                                    <span class="counter">3</span>
                                </a>
                            </div>
                            <div class="profile__btn">
                                <a href="#">
                            <span>
                                <svg class="icon icon-exit">
                                <use xlink:href="#icon-exit"></use>
                            </svg>
                            </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
