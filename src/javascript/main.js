/**
 * Created by htmlmak on 06.02.2018.
 */

$(document).ready(function () {
    // Scroll to block function
    $(function () {
        window.scrollToBlock = function (target) {
            if (!target.jquery) {
                target = $(target);
            }
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - 120
                }, 1000);
                return false;
            }
        };
    });

    // Change product count
    (function ($) {
        var changeCount = function (btn) {
            var $countBlock = btn.closest('.count');
            var currentValue = Number($countBlock.find('input').val());
            var newValue = null;

            // Up or down
            if (btn.find('.add').length) {
                newValue = currentValue + 1;
            } else {
                newValue = currentValue - 1;
            }

            if (newValue < 1) {
                newValue = 1
            }
            $countBlock.find('input')
                .val(newValue)
                .triggerHandler('changequantity', {
                    'new_value': newValue,
                    'old_value': currentValue
                })
            ;
        };

        $(document)
            .on('click.count', '.count__btn', function (e) {
                e.preventDefault();
                e.stopPropagation();

                changeCount($(this));
            })
            .on('change.count keyup.count', '.count input', function () {
                var newValue = Number(this.value.replace(/[^\d]/g, ''));

                if (newValue < 1) {
                    newValue = 1
                }

                this.value = newValue;
            })
            .on('cut copy paste', '.count input', function (event) {
                this.innerHTML = event.type + ' ' + this.value;
                return false;
            })
    })(jQuery);

    // Basket form UI
    (function ($) {
        var $changePersonalType = $('.change-personal-type');

        $changePersonalType.on('change', 'input', function () {
            if ($changePersonalType.find('input:checked').data('type') == 'individual') {
                $('#entity-fields').slideUp();
                $('#requisites-basket').find('input[name="company_name"]').prop('required', false);
            } else {
                $('#entity-fields').slideDown();
                $('#requisites-basket').find('input[name="company_name"]').prop('required', true);
            }
        });


        // File
        $('#order-requisites-file').on('change', function () {
            $('#requisites-file-name').text(this.files[0].name).fadeIn(200);
            $('#order-requisites-btn').find('span').text('Выбран файл:');

            /* TODO: временный костыль */
            if ($('#requisites-basket').find('input[name="company_name"]').val() !== '') {
                $('#requisites-basket').stop().slideUp();
            }
        });

        $('#order-requisites-btn').on('click', function () {
            $('#order-requisites-file').click();
        });

        // Requisites
        $('#write-requisites-open').on('click', function () {
            $('#requisites-basket').stop().slideToggle();
        });

        /* TODO: позже убрать */
        $('#register-basket-block button[type="submit"]').on('click', function (e) {
            if ($changePersonalType.find('input:checked').data('type') == 'entity' && $('#requisites-basket').find('input[name="company_name"]').val() == '') {
                $('#requisites-basket').stop().slideDown();
            }
        });


        $('#register-basket-block button[type="submit"]').on('click', function () {
            $('#register-basket-block form').addClass('touched');
        });
    })(jQuery);

    // Product detail
    (function ($) {
        $('.btn-more-wrapper').on('click', function (e) {
            e.preventDefault();
            $('#editions').find('.options-cards__list .column:nth-child(n+3)').slideDown();
            $(this).fadeOut(250);
        })
    })(jQuery);

    // Filters
    (function ($) {
        var checkboxes = $('#filter').find('.dropdown-pane input[type="checkbox"]');
        var getSerializeFilter = function () {
            var result = [];
            checkboxes.each(function () {
                if (this.checked) {
                    result.push(this.value);
                }
            });
            return result;
        };

        $('#filter .dropdown-pane').on('change', 'input[type="checkbox"]', function (e) {
            $('#filter').trigger('changeFilter', {
                filter: getSerializeFilter()
            });
        });

        $('#clear-filter').on('click', function () {
            checkboxes.prop('checked', false);

            $('#filter').trigger('changeFilter', {
                filter: getSerializeFilter()
            });
        });

        $('.filter__selected').on('click', 'button', function (e) {
            e.preventDefault();
            var _ = $(this);

            if (_.hasClass('close')) {
                checkboxes.prop('checked', false);
            } else {
                checkboxes
                    .filter(function () {
                        return this.value == _.data('value');
                    })
                    .prop('checked', false);
            }

            $('#filter').trigger('changeFilter', {
                filter: getSerializeFilter()
            });
        });
    })(jQuery);

    // Assign order to manager
    (function ($) {
        var $resultModal = $('#assign-order-modal');
        $(document).on('click', '.assign-order-btn', function (e) {
            e.preventDefault();
            $.ajax({
                url: e.target.href,
                type: 'GET',
                dataType: 'json',
                cache: false,
                success: function (data) {
                    var message = '';
                    if (data.error_code == '0') {
                        message = data.message
                    } else {
                        message = data.error_message
                    }
                    $resultModal.find('h4').text(message);
                    $resultModal.foundation('open');
                }
            })

        });
    })(jQuery);
});
