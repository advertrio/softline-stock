'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const gulpIf = require('gulp-if');
const argv = require('yargs').argv;
const rimraf = require('gulp-rimraf');
const sourcemaps = require('gulp-sourcemaps');
const notify = require('gulp-notify');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const imagemin = require('gulp-imagemin');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');
const plumber = require('gulp-plumber');
const svgSprite = require('gulp-svg-sprites');
const cssnano = require('cssnano');
const flexfix = require('postcss-flexbugs-fixes');
const spritesmith = require('gulp.spritesmith');
const zip = require('gulp-zip');
const pug = require('gulp-pug');
const svgmin = require('gulp-svgmin');


const isDevelopment = !argv.production;
const config = {
    coreDir: './',
    destDir: 'dist',
    sourceDir: 'src'
};

const compeletePage = [
    './index.php'
];

let customFoundationCore = [
    'node_modules/foundation-sites/dist/js/foundation.min.js',
];

let conf_postcss = {
    default: [
        autoprefixer({browsers: ['last 2 versions', 'ie >= 9']}),
        flexfix
    ],

    prod: [
        autoprefixer({browsers: ['last 2 versions', 'ie >= 9']}),
        flexfix,
        cssnano({preset: 'default'})
    ]

};


gulp.task('styles', function () {
    return gulp.src('src/scss/template_styles.scss')
        .pipe(plumber({errorHandler: notify.onError({title: "styles"})}))
        .pipe(gulpIf(isDevelopment, sourcemaps.init()))
        .pipe(sass())
        .pipe(postcss(
            gulpIf(isDevelopment, conf_postcss.default, conf_postcss.prod)
        ))
        .pipe(gulpIf(isDevelopment, sourcemaps.write()))
        .pipe(gulp.dest(config.destDir + '/css'))
});

gulp.task('pug', function buildHTML() {
    return gulp.src(config.sourceDir + '/*.{pug,jade}')
        .pipe(pug({
            // Your options in here.
        }))
        .pipe(gulp.dest(config.coreDir))
});

gulp.task('clean', function () {
    return gulp.src(['images*', 'css*', 'fonts*'], {read: false}) // much faster
        .pipe(rimraf());
});

// Создание спрайтов
gulp.task('sprite-create', function () {
    var fileName = 'icons'; //'sprite-' + Math.random().toString().replace(/[^0-9]/g, '');

    var spriteData = gulp.src(config.sourceDir + '/images/sprite/*.png')
        .pipe(spritesmith({
            cssName: '_sprite.scss',
            cssFormat: 'scss',
            cssVarMap: function (sprite) {
                sprite.name = 'icon-' + sprite.name.replace('@', '-');
            },
            imgName: fileName + '.png',
            imgPath: '../images/' + fileName + '.png',
        }));

    spriteData.img
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}]
        }))
        .pipe(gulp.dest(config.destDir + '/images'));

    spriteData.css
        .pipe(gulp.dest(config.sourceDir + '/scss/components/'));

    return spriteData;
});

gulp.task('java-script', function () {
    let src = [config.sourceDir + '/javascript/jquery.min.js'].concat(customFoundationCore,
        [
            config.sourceDir + '/javascript/owl.carousel.min.js',
            config.sourceDir + '/javascript/jquery.nice-select.min.js',
            config.sourceDir + '/javascript/modernizr-custom.js',
            config.sourceDir + '/javascript/main.js'
        ]);
    return gulp.src(src)
        .pipe(plumber({errorHandler: notify.onError({title: "java-script"})}))
        .pipe(concat('bundle.js'))
        .pipe(gulpIf(!isDevelopment, uglify()))
        .pipe(gulp.dest(config.destDir + '/javascript'));
});


gulp.task('images', function () {
    return gulp.src(['src/images/*.{jpg,png}', 'src/images/**/*.{jpg,png}', '!src/images/svg/', '!src/images/sprite/'])
        .pipe(plumber({errorHandler: notify.onError({title: "images"})}))
        .pipe(gulpIf(!isDevelopment, imagemin({progressive: true})))
        .pipe(gulp.dest(config.destDir + '/images'));

});

gulp.task('svg-sprite', function () {
    return gulp.src('src/{img,images}/svg/*.svg')
        .pipe(plumber({errorHandler: notify.onError({title: "svg-sprite"})}))
        .pipe(svgmin({
            plugins: [{
                removeDoctype: false
            }, {
                removeXMLNS: true
            }, {
                removeXMLProcInst: true
            }, {
                removeComments: true
            }, {
                removeMetadata: true
            }, {
                removeEditorNSData: true
            },  {
                cleanupNumericValues: {
                    floatPrecision: 2
                }
            }, {
                removeViewBox: false
            }]
        }))
        .pipe(svgSprite({
            mode: "symbols",
            svgId: "icon-%f",
        }))
        .pipe(gulp.dest(config.destDir + '/images/icons/'));
});

gulp.task('fonts', function () {
    return gulp.src(config.sourceDir + '/fonts/*.*', config.sourceDir + '/fonts/**/*.*')
        .pipe(gulp.dest(config.destDir + '/fonts'));
});


gulp.task('zip', function () {
    const zipName = 'static_build_latest.zip';
    let src = [config.destDir + '/**', '!dist/*.zip', 'partials', 'partials/**'];
    src = src.concat(compeletePage);

    return gulp.src(src, {base: __dirname})
        .pipe(zip(zipName))
        .pipe(gulp.dest(config.destDir));
});

gulp.task('build', gulp.series('clean', 'sprite-create', gulp.parallel('styles', 'images', 'svg-sprite', 'java-script', 'fonts')));

gulp.task('watch', function () {
    //gulp.watch(['src/scss/*.scss', 'src/scss/pages/*.scss', 'src/scss/components/blocks/**/*.scss', 'src/scss/components/**/*.scss', 'src/scss/foundation/**/*.scss'], gulp.series('styles'));
    gulp.watch(['src/scss/*.scss', 'src/scss/**/*.scss'], gulp.series('styles'));
    gulp.watch(['src/{img,images}/*.{jpg,png}', 'src/{img,images}/**/*.{jpg,png}', '!src/{img,images}/svg/'], gulp.series('images'));
    gulp.watch('src/{img,images}/svg/*.svg', gulp.series('svg-sprite'));
    gulp.watch('src/{img,images}/sprite/*.png', gulp.series('sprite-create'));
    gulp.watch('src/{js,script,java,javascript}/*.js', gulp.series('java-script'));
    gulp.watch('src/fonts/*.{ttf,woff,woff2,eot}', gulp.series('fonts'));
    //gulp.watch('src/*.{pug,jade}', gulp.series('pug'));
});


gulp.task('dev', gulp.series('build', 'watch'));