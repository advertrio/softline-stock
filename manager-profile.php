<? $title = 'Profile' ?>
<? include 'partials/header.php' ?>
<nav class="navigation show-for-large">
    <div class="row">
        <div class="column small-12">
            <ul class="navigation__menu">
                <li><a href="#">О продукте</a></li>
                <li><a href="#">Комплектация</a></li>
                <li><a href="#">специальные</a></li>
                <li><a href="#">предложения </a></li>
                <li><a href="#">Подробное описание</a></li>
                <li><a href="#">Мероприятия</a></li>
            </ul>
        </div>
    </div>
</nav>
<main class="main -padding-y">
    <section style="padding-bottom: 30px">
        <div class="row">
            <div class="column small-12 medium-6 text-center medium-text-left">
                <div class="b-user -large-offset">
                    <div class="b-user__photo">
                        <a href="#">
                            <svg class="icon icon-user">
                                <use xlink:href="#icon-user"></use>
                            </svg>
                        </a>
                    </div>
                    <div class="b-user__name">
                        <a href="#">Иванов Иван Иванович</a>
                    </div>
                </div>
            </div>
            <div class="column small-12 medium-6 text-center large-text-right">
                <a href="#" class="button alternate no-shadow text-tiny">заказать активность</a>
            </div>
        </div>
    </section>
    <section>
        <div class="row">
            <div class="column small-12">
                <div class="b-title -margin-y -large-offset">
                    <span>Мои заказы</span>
                </div>
            </div>
            <div class="column small-12">

                <div class="content">
                    <div class="table-row">

                        <div class="table-column -product expand">
                            <div class="product-item">
                                <div class="product-item__photo">
                                    <a href="#"><img src="content/product-item/product-img-1.png" alt=""></a>
                                </div>
                                <div class="product-item__content">
                                    <div class="product-item__title">
                                        <a href="#"><span>Microsoft azure</span></a>
                                    </div>
                                    <div class="product-item__subtitle">
                                        <span>комплектация</span>
                                    </div>
                                    <div class="text-price -basket show-for-small-only">
                                        <p class="large"><strong>10 000</strong><strong>₽</strong></p>
                                    </div>
                                    <div class="product-item__desc">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea com
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="table-column show-for-medium">
                            <div class="text-price -basket">
                                <p class="large"><strong>10 000</strong><strong>₽</strong></p>
                            </div>
                        </div>
                    </div>
                    <div class="table-row">

                        <div class="table-column -product expand">
                            <div class="product-item">
                                <div class="product-item__photo">
                                    <a href="#"><img src="content/product-item/product-img-2.png" alt=""></a>
                                </div>
                                <div class="product-item__content">
                                    <div class="product-item__title">
                                        <a href="#"><span>Microsoft azure</span></a>
                                    </div>
                                    <div class="product-item__subtitle">
                                        <span>комплектация</span>
                                    </div>
                                    <div class="text-price -basket show-for-small-only">
                                        <p class="large"><strong>10 000</strong><strong>₽</strong></p>
                                    </div>
                                    <div class="product-item__desc">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea com
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="table-column show-for-medium">
                            <div class="text-price -basket">
                                <p class="large"><strong>10 000</strong><strong>₽</strong></p>
                            </div>
                        </div>
                    </div>
                    <div class="table-row">

                        <div class="table-column -product expand">
                            <div class="product-item">
                                <div class="product-item__photo">
                                    <a href="#"><img src="content/product-item/product-img-3.png" alt=""></a>
                                </div>
                                <div class="product-item__content">
                                    <div class="product-item__title">
                                        <a href="#"><span>Microsoft azure</span></a>
                                    </div>
                                    <div class="product-item__subtitle">
                                        <span>комплектация</span>
                                    </div>
                                    <div class="text-price -basket show-for-small-only">
                                        <p class="large"><strong>10 000</strong><strong>₽</strong></p>
                                    </div>
                                    <div class="product-item__desc">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea com
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="table-column show-for-medium">
                            <div class="text-price -basket">
                                <p class="large"><strong>10 000</strong><strong>₽</strong></p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section>
        <div class="row">
            <div class="column small-12">
                <div class="b-title -margin-y -large-offset">
                    <span>Интересные проекты</span>
                </div>
            </div>
        </div>

        <div class="row small-up-1 medium-up-2 large-up-3">
            <div class="column">
                <div class="platform-card filled-true number-true">
                    <div class="platform-card__number">
                        <span>44 шт.</span>
                    </div>
                    <div style="background-image: url('content/profile-card/microsoft.png')" class="platform-card__top">
                        <div class="platform-card__text">
                            <p>Microsoft <br> azure</p>
                            <p><span>комплектация</span></p>
                            <span><a class="platform-mobile-btn button tiny" href="#">смотреть</a></span>
                        </div>

                        <div class="platform-card__logo">
                            <span><img src="dist/images/platform-card/microsoft.png" alt=""></span>
                        </div>
                        <div class="platform-card__button">
                            <span><a class="button" href="#">смотреть</a></span>
                        </div>
                    </div>
                    <div class="platform-card__bottom">
                        <div class="platform-card__price">
                            <p><span>5 290</span><span>₽</span></p>
                        </div>
                        <span><a class="button" href="#">купить</a></span>
                        <div class="platform-card__manager-info">
                            <div class="m-info filled">
                                <svg class="icon icon-check">
                                    <use xlink:href="#icon-check"></use>
                                </svg>
                                <span>реквизиты</span>
                            </div>
                            <div class="m-info assigned">
                                <svg class="icon icon-user">
                                    <use xlink:href="#icon-user"></use>
                                </svg>
                                <span>Менеджер назначен</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="column">
                <div class="platform-card">
                    <div class="platform-card__number">
                        <span>44 шт.</span>
                    </div>
                    <div style="background-image: url('content/profile-card/ibm.png')" class="platform-card__top">
                        <div class="platform-card__text">
                            <p>IBM Aspera</p>
                            <p><span>комплектация</span></p>
                            <span><a class="platform-mobile-btn button tiny" href="#">смотреть</a></span>
                        </div>

                        <div class="platform-card__logo">
                            <span><img src="dist/images/platform-card/ibm.png" alt=""></span>
                        </div>
                        <div class="platform-card__button">
                            <span><a class="button" href="#">смотреть</a></span>
                        </div>
                    </div>
                    <div class="platform-card__bottom">
                        <div class="platform-card__price">
                            <p><span>5 290</span><span>₽</span></p>
                        </div>
                        <span><a class="button" href="#">купить</a></span>
                        <div class="platform-card__manager-info">
                            <div class="m-info filled">
                                <svg class="icon icon-check">
                                    <use xlink:href="#icon-check"></use>
                                </svg>
                                <span>реквизиты</span>
                            </div>
                            <div class="m-info assigned">
                                <svg class="icon icon-user">
                                    <use xlink:href="#icon-user"></use>
                                </svg>
                                <span>Менеджер назначен</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="column">
                <div class="platform-card filled-true number-true">
                    <div class="platform-card__number">
                        <span>44 шт.</span>
                    </div>
                    <div style="background-image: url('content/profile-card/azure.png')" class="platform-card__top">
                        <div class="platform-card__text">
                            <p>Microsoft Azure</p>
                            <p><span>комплектация</span></p>
                            <span><a class="platform-mobile-btn button tiny" href="#">смотреть</a></span>
                        </div>

                        <div class="platform-card__logo">
                            <span><img src="dist/images/platform-card/microsoft.png" alt=""></span>
                        </div>
                        <div class="platform-card__button">
                            <span><a class="button" href="#">смотреть</a></span>
                        </div>
                    </div>
                    <div class="platform-card__bottom">
                        <div class="platform-card__price">
                            <p><span>5 290</span><span>₽</span></p>
                        </div>
                        <span><a class="button" href="#">купить</a></span>
                        <div class="platform-card__manager-info">
                            <div class="m-info filled">
                                <svg class="icon icon-check">
                                    <use xlink:href="#icon-check"></use>
                                </svg>
                                <span>реквизиты</span>
                            </div>
                            <div class="m-info assigned">
                                <svg class="icon icon-user">
                                    <use xlink:href="#icon-user"></use>
                                </svg>
                                <span>Менеджер назначен</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</main>
<? include 'partials/footer.php' ?>
<script>
    $(document).foundation();
</script>

</body>
</html>

