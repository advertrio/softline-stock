<? $title = 'Витрина менеджера' ?>
<? include 'partials/header.php' ?>
<main>
    <section class="filter">
        <div class="row small-collapse large-uncollapse">
            <div class="column small-12">
                <div class="filter__wrapper">
                    <div class="filter__options">
                        <button>ВСЕ</button>
                        <button class="btn-dropdown" type="button" data-toggle="company">компании</button>
                        <button class="btn-dropdown" type="button" data-toggle="products">продукты</button>
                        <div class="btn-pane dropdown-pane scrollbar-dynamic" id="company" data-dropdown
                             data-auto-focus="true">
                            <form action="">
                                <label class="b-checkbox"><input type="checkbox">
                                    <span>
                                    Microsft
                                    <svg class="icon icon-check"><use xlink:href="#icon-check"></use></svg>
                                </span>
                                </label>
                                <label class="b-checkbox"><input type="checkbox">
                                    <span>
                                    Apple
                                    <svg class="icon icon-check"><use xlink:href="#icon-check"></use></svg>
                                </span>
                                </label>
                                <label class="b-checkbox"><input type="checkbox">
                                    <span>
                                    IBM
                                    <svg class="icon icon-check"><use xlink:href="#icon-check"></use></svg>
                                </span>
                                </label>
                                <label class="b-checkbox"><input type="checkbox">
                                    <span>
                                   Google
                                    <svg class="icon icon-check"><use xlink:href="#icon-check"></use></svg>
                                </span>
                                </label>
                                <label class="b-checkbox"><input type="checkbox">
                                    <span>
                                   PTC
                                    <svg class="icon icon-check"><use xlink:href="#icon-check"></use></svg>
                                </span>
                                </label>
                                <label class="b-checkbox"><input type="checkbox">
                                    <span>
                                   Oracle
                                    <svg class="icon icon-check"><use xlink:href="#icon-check"></use></svg>
                                </span>
                                </label>
                                <label class="b-checkbox"><input type="checkbox">
                                    <span>
                                   Paesler
                                    <svg class="icon icon-check"><use xlink:href="#icon-check"></use></svg>
                                </span>
                                </label>
                                <label class="b-checkbox"><input type="checkbox">
                                    <span>
                                  Vmware
                                    <svg class="icon icon-check"><use xlink:href="#icon-check"></use></svg>
                                </span>
                                </label>
                                <label class="b-checkbox"><input type="checkbox">
                                    <span>
                                  Intel
                                    <svg class="icon icon-check"><use xlink:href="#icon-check"></use></svg>
                                </span>
                                </label>
                                <label class="b-checkbox"><input type="checkbox">
                                    <span>
                                   Softline
                                    <svg class="icon icon-check"><use xlink:href="#icon-check"></use></svg>
                                </span>
                                </label>
                            </form>
                        </div>
                        <div class="btn-pane dropdown-pane" id="products" data-dropdown data-auto-focus="true">
                            <form action="">
                                <label class="b-checkbox"><input type="checkbox">
                                    <span>
                                    Microsft
                                    <svg class="icon icon-check"><use xlink:href="#icon-check"></use></svg>
                                </span>
                                </label>
                                <label class="b-checkbox"><input type="checkbox">
                                    <span>
                                    Apple
                                    <svg class="icon icon-check"><use xlink:href="#icon-check"></use></svg>
                                </span>
                                </label>
                                <label class="b-checkbox"><input type="checkbox">
                                    <span>
                                    IBM
                                    <svg class="icon icon-check"><use xlink:href="#icon-check"></use></svg>
                                </span>
                                </label>
                                <label class="b-checkbox"><input type="checkbox">
                                    <span>
                                   Google
                                    <svg class="icon icon-check"><use xlink:href="#icon-check"></use></svg>
                                </span>
                                </label>
                                <label class="b-checkbox"><input type="checkbox">
                                    <span>
                                   PTC
                                    <svg class="icon icon-check"><use xlink:href="#icon-check"></use></svg>
                                </span>
                                </label>
                                <label class="b-checkbox"><input type="checkbox">
                                    <span>
                                   Oracle
                                    <svg class="icon icon-check"><use xlink:href="#icon-check"></use></svg>
                                </span>
                                </label>
                                <label class="b-checkbox"><input type="checkbox">
                                    <span>
                                   Paesler
                                    <svg class="icon icon-check"><use xlink:href="#icon-check"></use></svg>
                                </span>
                                </label>
                                <label class="b-checkbox"><input type="checkbox">
                                    <span>
                                  Vmware
                                    <svg class="icon icon-check"><use xlink:href="#icon-check"></use></svg>
                                </span>
                                </label>
                                <label class="b-checkbox"><input type="checkbox">
                                    <span>
                                  Intel
                                    <svg class="icon icon-check"><use xlink:href="#icon-check"></use></svg>
                                </span>
                                </label>
                                <label class="b-checkbox"><input type="checkbox">
                                    <span>
                                   Softline
                                    <svg class="icon icon-check"><use xlink:href="#icon-check"></use></svg>
                                </span>
                                </label>
                            </form>
                        </div>
                    </div>
                    <div class="filter__selected">
                        <button><span>IMB</span>
                            <svg class="icon icon-close">
                                <use xlink:href="#icon-close"></use>
                            </svg>
                        </button>
                        <button><span>adobe</span>
                            <svg class="icon icon-close">
                                <use xlink:href="#icon-close"></use>
                            </svg>
                        </button>
                        <button><span>Облачные технологии</span>
                            <svg class="icon icon-close">
                                <use xlink:href="#icon-close"></use>
                            </svg>
                        </button>
                        <button class="close">
                            <span>очистить все</span>
                            <svg class="icon icon-close">
                                <use xlink:href="#icon-close"></use>
                            </svg>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="platform-list">
        <div class="row small-up-1 medium-up-2 large-up-3">
            <div class="column">
                <div class="platform-card filled-true number-true">
                    <div class="platform-card__number">
                        <span>44 шт.</span>
                    </div>
                    <div style="background-image: url('content/profile-card/microsoft.png')" class="platform-card__top">
                        <div class="platform-card__text">
                            <p>Microsoft <br> azure</p>
                            <p><span>комплектация</span></p>
                            <span><a class="platform-mobile-btn button tiny" href="#">смотреть</a></span>
                        </div>

                        <div class="platform-card__logo">
                            <span><img src="dist/images/platform-card/microsoft.png" alt=""></span>
                        </div>
                        <div class="platform-card__button">
                            <span><a class="button" href="#">смотреть</a></span>
                        </div>
                    </div>
                    <div class="platform-card__bottom">
                            <div class="platform-card__price">
                                <p><span>5 290</span><span>₽</span></p>
                            </div>
                        <span><a class="button" href="#">купить</a></span>
                        <div class="platform-card__manager-info">
                            <div class="m-info filled">
                                <svg class="icon icon-check">
                                    <use xlink:href="#icon-check"></use>
                                </svg>
                                <span>реквизиты</span>
                            </div>
                            <div class="m-info assigned">
                                <svg class="icon icon-user">
                                    <use xlink:href="#icon-user"></use>
                                </svg>
                                <span>Менеджер назначен</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="column">
                    <div class="platform-card">
                    <div class="platform-card__number">
                        <span>44 шт.</span>
                    </div>
                    <div style="background-image: url('content/profile-card/ibm.png')" class="platform-card__top">
                        <div class="platform-card__text">
                            <p>IBM Aspera</p>
                            <p><span>комплектация</span></p>
                                 <span><a class="platform-mobile-btn button tiny" href="#">смотреть</a></span>
                        </div>

                        <div class="platform-card__logo">
                            <span><img src="dist/images/platform-card/ibm.png" alt=""></span>
                        </div>
                        <div class="platform-card__button">
                            <span><a class="button" href="#">смотреть</a></span>
                        </div>
                    </div>
                                <div class="platform-card__bottom">
                            <div class="platform-card__price">
                                <p><span>5 290</span><span>₽</span></p>
                            </div>
                        <span><a class="button" href="#">купить</a></span>
                        <div class="platform-card__manager-info">
                            <div class="m-info filled">
                                <svg class="icon icon-check">
                                    <use xlink:href="#icon-check"></use>
                                </svg>
                                <span>реквизиты</span>
                            </div>
                            <div class="m-info assigned">
                                <svg class="icon icon-user">
                                    <use xlink:href="#icon-user"></use>
                                </svg>
                                <span>Менеджер назначен</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="column">
                    <div class="platform-card filled-true number-true">
                    <div class="platform-card__number">
                        <span>44 шт.</span>
                    </div>
                    <div style="background-image: url('content/profile-card/azure.png')" class="platform-card__top">
                        <div class="platform-card__text">
                            <p>Microsoft Azure</p>
                            <p><span>комплектация</span></p>
                                 <span><a class="platform-mobile-btn button tiny" href="#">смотреть</a></span>
                        </div>

                        <div class="platform-card__logo">
                            <span><img src="dist/images/platform-card/microsoft.png" alt=""></span>
                        </div>
                        <div class="platform-card__button">
                            <span><a class="button" href="#">смотреть</a></span>
                        </div>
                    </div>
                                <div class="platform-card__bottom">
                            <div class="platform-card__price">
                                <p><span>5 290</span><span>₽</span></p>
                            </div>
                        <span><a class="button" href="#">купить</a></span>
                        <div class="platform-card__manager-info">
                            <div class="m-info filled">
                                <svg class="icon icon-check">
                                    <use xlink:href="#icon-check"></use>
                                </svg>
                                <span>реквизиты</span>
                            </div>
                            <div class="m-info assigned">
                                <svg class="icon icon-user">
                                    <use xlink:href="#icon-user"></use>
                                </svg>
                                <span>Менеджер назначен</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="column">
                    <div class="platform-card filled-true">
                    <div class="platform-card__number">
                        <span>44 шт.</span>
                    </div>
                    <div style="background-image: url('content/profile-card/minitab.png')" class="platform-card__top">
                        <div class="platform-card__text">
                            <p>Minitab 18</p>
                            <p><span>комплектация</span></p>
                                 <span><a class="platform-mobile-btn button tiny" href="#">смотреть</a></span>
                        </div>

                        <div class="platform-card__logo">
                            <span><img src="dist/images/platform-card/minitab-logo.png" alt=""></span>
                        </div>
                        <div class="platform-card__button">
                            <span><a class="button" href="#">смотреть</a></span>
                        </div>
                    </div>
                                <div class="platform-card__bottom">
                            <div class="platform-card__price">
                                <p><span>5 290</span><span>₽</span></p>
                            </div>
                        <span><a class="button" href="#">купить</a></span>
                        <div class="platform-card__manager-info">
                            <div class="m-info filled">
                                <svg class="icon icon-check">
                                    <use xlink:href="#icon-check"></use>
                                </svg>
                                <span>реквизиты</span>
                            </div>
                            <div class="m-info assigned">
                                <svg class="icon icon-user">
                                    <use xlink:href="#icon-user"></use>
                                </svg>
                                <span>Менеджер назначен</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="column">
                    <div class="platform-card">
                    <div class="platform-card__number">
                        <span>44 шт.</span>
                    </div>
                    <div style="background-image: url('content/profile-card/PTC.png')" class="platform-card__top">
                        <div class="platform-card__text">
                            <p>PTC Mathcad
                                <br> Prime 4.0</p>
                            <p><span>комплектация</span></p>
                                 <span><a class="platform-mobile-btn button tiny" href="#">смотреть</a></span>
                        </div>

                        <div class="platform-card__logo">
                            <span><img src="dist/images/platform-card/ptc-logo.png" alt=""></span>
                        </div>
                        <div class="platform-card__button">
                            <span><a class="button" href="#">смотреть</a></span>
                        </div>
                    </div>
                                <div class="platform-card__bottom">
                            <div class="platform-card__price">
                                <p><span>5 290</span><span>₽</span></p>
                            </div>
                        <span><a class="button" href="#">купить</a></span>
                        <div class="platform-card__manager-info">
                            <div class="m-info filled">
                                <svg class="icon icon-check">
                                    <use xlink:href="#icon-check"></use>
                                </svg>
                                <span>реквизиты</span>
                            </div>
                            <div class="m-info assigned">
                                <svg class="icon icon-user">
                                    <use xlink:href="#icon-user"></use>
                                </svg>
                                <span>Менеджер назначен</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="column">
                    <div class="platform-card filled-true  number-true">
                    <div class="platform-card__number">
                        <span>44 шт.</span>
                    </div>
                    <div style="background-image: url('content/profile-card/oracle.png')" class="platform-card__top">
                        <div class="platform-card__text">
                            <p>PTC Mathcad
                                <br> Prime 4.0</p>
                            <p><span>Oracle Cloud MSP</span></p>
                        </div>

                        <div class="platform-card__logo">
                            <span><img src="dist/images/platform-card/oracle-logo.png" alt=""></span>
                        </div>
                        <div class="platform-card__button">
                            <span><a class="button" href="#">смотреть</a></span>
                        </div>
                    </div>
                                <div class="platform-card__bottom">
                            <div class="platform-card__price">
                                <p><span>5 290</span><span>₽</span></p>
                            </div>
                        <span><a class="button" href="#">купить</a></span>
                        <div class="platform-card__manager-info">
                            <div class="m-info filled">
                                <svg class="icon icon-check">
                                    <use xlink:href="#icon-check"></use>
                                </svg>
                                <span>реквизиты</span>
                            </div>
                            <div class="m-info assigned">
                                <svg class="icon icon-user">
                                    <use xlink:href="#icon-user"></use>
                                </svg>
                                <span>Менеджер назначен</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="column">
                    <div class="platform-card filled-true assigned-true">
                    <div class="platform-card__number">
                        <span>44 шт.</span>
                    </div>
                    <div style="background-image: url('content/profile-card/google.png')" class="platform-card__top">
                        <div class="platform-card__text">
                            <p>G Suite
                                <br> (ex. Google Apps)</p>
                            <p><span>Oracle Cloud MSP</span></p>
                        </div>

                        <div class="platform-card__logo">
                            <span><img src="dist/images/platform-card/google-logo.png" alt=""></span>
                        </div>
                        <div class="platform-card__button">
                            <span><a class="button" href="#">смотреть</a></span>
                        </div>
                    </div>
                                <div class="platform-card__bottom">
                            <div class="platform-card__price">
                                <p><span>5 290</span><span>₽</span></p>
                            </div>
                        <span><a class="button" href="#">купить</a></span>
                        <div class="platform-card__manager-info">
                            <div class="m-info filled">
                                <svg class="icon icon-check">
                                    <use xlink:href="#icon-check"></use>
                                </svg>
                                <span>реквизиты</span>
                            </div>
                            <div class="m-info assigned">
                                <svg class="icon icon-user">
                                    <use xlink:href="#icon-user"></use>
                                </svg>
                                <span>Менеджер назначен</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="column">
                    <div class="platform-card filled-true assigned-true">
                    <div class="platform-card__number">
                        <span>44 шт.</span>
                    </div>
                    <div style="background-image: url('content/profile-card/prtg.png')" class="platform-card__top">
                        <div class="platform-card__text">
                            <p>PRTG <br> Network Monitor</p>
                            <p><span>Oracle Cloud MSP</span></p>
                        </div>

                        <div class="platform-card__logo">
                            <span><img src="dist/images/platform-card/paessler-logo.png" alt=""></span>
                        </div>
                        <div class="platform-card__button">
                            <span><a class="button" href="#">смотреть</a></span>
                        </div>
                    </div>
                                <div class="platform-card__bottom">
                            <div class="platform-card__price">
                                <p><span>5 290</span><span>₽</span></p>
                            </div>
                        <span><a class="button" href="#">купить</a></span>
                        <div class="platform-card__manager-info">
                            <div class="m-info filled">
                                <svg class="icon icon-check">
                                    <use xlink:href="#icon-check"></use>
                                </svg>
                                <span>реквизиты</span>
                            </div>
                            <div class="m-info assigned">
                                <svg class="icon icon-user">
                                    <use xlink:href="#icon-user"></use>
                                </svg>
                                <span>Менеджер назначен</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="column">
                    <div class="platform-card filled-true assigned-true number-true">
                    <div class="platform-card__number">
                        <span>44 шт.</span>
                    </div>
                    <div style="background-image: url('content/profile-card/vmvare.png')" class="platform-card__top">
                        <div class="platform-card__text">
                            <p>VMWARE HORIZON 7</p>
                            <p><span>комплектация</span></p>
                                 <span><a class="platform-mobile-btn button tiny" href="#">смотреть</a></span>
                        </div>

                        <div class="platform-card__logo">
                            <span><img src="dist/images/platform-card/microsoft.png" alt=""></span>
                        </div>
                        <div class="platform-card__button">
                            <span><a class="button" href="#">смотреть</a></span>
                        </div>
                    </div>
                    <div class="platform-card__bottom">
                        <div class="platform-card__price">
                            <p><span>11 000</span><span>₽</span></p>
                            <p><span class="old-price">12 500</span><span class="old-price">₽</span></p>
                        </div>
                        <span><a class="button" href="#">купить</a></span>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="row">
            <div class="column">
                <div class="pagination-wrapper">
                    <ul class="pagination" role="navigation" aria-label="Pagination">
                        <li class="pagination-previous disabled">
                            <span>
                                <svg class="icon icon-pg-arrow"><use xlink:href="#icon-pg-arrow"></use></svg>
                            </span>
                        </li>
                        <li class="current"><span> 1</span></li>
                        <li><a href="#" aria-label="Page 2">2</a></li>
                        <li><a href="#" aria-label="Page 3">3</a></li>
                        <li><a href="#" aria-label="Page 4">4</a></li>
                        <li class="ellipsis" aria-hidden="true"></li>
                        <li><a href="#" aria-label="Page 12">12</a></li>
                        <li><a href="#" aria-label="Page 13">13</a></li>
                        <li class="pagination-next"><a href="#" aria-label="Next page">
                                   <span>
                                <svg class="icon icon-pg-arrow"><use xlink:href="#icon-pg-arrow"></use></svg>
                            </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
</main>
<? include 'partials/footer.php' ?>
<script>
    $('.top-slider__carousel').owlCarousel({
        nav: true,
        dots: true,
        loop: true,
        navText: [$('.top-slider__nav.-prev'), $('.top-slider__nav.-next')],
        navContainer: '.top-slider__arrow-wrapper',
        items: 1
    });

    $(document).foundation();
</script>
</body>
</html>