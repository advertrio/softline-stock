<? $title = 'Корзина' ?>
<? include 'partials/header.php' ?>
<nav class="navigation show-for-large">
    <div class="row">
        <div class="column small-12">
            <ul class="navigation__menu">
                <li><a href="#">О продукте</a></li>
                <li><a href="#">Комплектация</a></li>
                <li><a href="#">специальные</a></li>
                <li><a href="#">предложения </a></li>
                <li><a href="#">Подробное описание</a></li>
                <li><a href="#">Мероприятия</a></li>
            </ul>
        </div>
    </div>
</nav>
<main class="main -padding-y">

    <section>
        <div class="row">
            <div class="column small-12">
                <div class="b-title -margin-y -large-offset">
                    <span>Корзина</span>
                </div>
            </div>
            <div class="column small-12">
                <div class="basket">
                    <div class="basket__top">
                        <div class="table-row" data-closable>

                            <div class="table-column -product expand">
                                <div class="product-item">
                                    <div class="product-item__photo">
                                        <a href="#"><img src="content/product-item/product-img-1.png" alt=""></a>
                                    </div>
                                    <div class="product-item__content">
                                        <div class="product-item__title">
                                            <a href="#"><span>Microsoft azure</span></a>
                                            <div class="close-btn-wrap text-right">
                                                <button class="close-button" aria-label="Dismiss alert"
                                                        type="button"
                                                        data-close>
                                                    <svg class="icon icon-close">
                                                        <use xlink:href="#icon-close"></use>
                                                    </svg>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="product-item__subtitle">
                                            <span>комплектация</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="table-column -count">
                                <div class="count -gray">
                                    <button class="count__btn"><span class="add"></span></button>
                                    <input value="2" type="text">
                                    <button class="count__btn"><span class="remove"></span></button>
                                </div>
                            </div>
                            <div class="table-column -price">
                                <div class="text-price -basket">
                                    <p class="large"><strong>10 000</strong><strong>₽</strong></p>
                                    <p class="large"><span>15 000</span><span>₽</span></p>
                                </div>
                            </div>

                            <div class="table-column -close">
                                <div class="close-btn-wrap text-right">
                                    <button class="close-button" aria-label="Dismiss alert" type="button"
                                            data-close>
                                        <svg class="icon icon-close">
                                            <use xlink:href="#icon-close"></use>
                                        </svg>
                                    </button>
                                </div>

                            </div>

                        </div>
                        <div class="table-row" data-closable>

                            <div class="table-column -product expand">
                                <div class="product-item">
                                    <div class="product-item__photo">
                                        <a href="#"><img src="content/product-item/product-img-2.png" alt=""></a>
                                    </div>
                                    <div class="product-item__content">
                                        <div class="product-item__title">
                                            <a href="#"><span>Microsoft azure</span></a>
                                            <div class="close-btn-wrap text-right">
                                                <button class="close-button" aria-label="Dismiss alert"
                                                        type="button"
                                                        data-close>
                                                    <svg class="icon icon-close">
                                                        <use xlink:href="#icon-close"></use>
                                                    </svg>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="product-item__subtitle">
                                            <span>комплектация</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="table-column -count">
                                <div class="count -gray">
                                    <button class="count__btn"><span class="add"></span></button>
                                    <input value="2" type="text">
                                    <button class="count__btn"><span class="remove"></span></button>
                                </div>
                            </div>
                            <div class="table-column -price">
                                <div class="text-price -basket">
                                    <p class="large"><strong>10 000</strong><strong>₽</strong></p>
                                    <p class="large"><span>15 000</span><span>₽</span></p>
                                </div>
                            </div>

                            <div class="table-column -close">
                                <div class="close-btn-wrap text-right">
                                    <button class="close-button" aria-label="Dismiss alert" type="button"
                                            data-close>
                                        <svg class="icon icon-close">
                                            <use xlink:href="#icon-close"></use>
                                        </svg>
                                    </button>
                                </div>

                            </div>

                        </div>
                        <div class="table-row" data-closable>

                            <div class="table-column -product expand">
                                <div class="product-item">
                                    <div class="product-item__photo">
                                        <a href="#"><img src="content/product-item/product-img-3.png" alt=""></a>
                                    </div>
                                    <div class="product-item__content">
                                        <div class="product-item__title">
                                            <a href="#"><span>Microsoft azure</span></a>
                                            <div class="close-btn-wrap text-right">
                                                <button class="close-button" aria-label="Dismiss alert"
                                                        type="button"
                                                        data-close>
                                                    <svg class="icon icon-close">
                                                        <use xlink:href="#icon-close"></use>
                                                    </svg>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="product-item__subtitle">
                                            <span>комплектация</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="table-column -count">
                                <div class="count -gray">
                                    <button class="count__btn"><span class="add"></span></button>
                                    <input value="2" type="text">
                                    <button class="count__btn"><span class="remove"></span></button>
                                </div>
                            </div>
                            <div class="table-column -price">
                                <div class="text-price -basket">
                                    <p class="large"><strong>10 000</strong><strong>₽</strong></p>
                                    <p class="large"><span>15 000</span><span>₽</span></p>
                                </div>
                            </div>

                            <div class="table-column -close">
                                <div class="close-btn-wrap text-right">
                                    <button class="close-button" aria-label="Dismiss alert" type="button"
                                            data-close>
                                        <svg class="icon icon-close">
                                            <use xlink:href="#icon-close"></use>
                                        </svg>
                                    </button>
                                </div>

                            </div>

                        </div>
                    </div>
                    <div class="basket__bottom">
                        <div class="promo-input small-margin">
                            <input placeholder="Ввести промокод" type="text">
                        </div>
                        <div class="basket__price small-margin">
                            <p>итого: <span>33 000₽</span></p>
                        </div>
                        <button onclick="$('#register-basket-block').slideDown(); scrollToBlock('#register-basket-block')" class="button">выставить счет
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="register-basket-block" style="display: none">
        <form class="row">
            <div class="column small-12">
                <div class="ordering-wrapper">
                    <div class="row">
                        <div class="column small-12">
                            <div class="b-title -margin-y">
                                <span>Оформление заказа</span>
                            </div>
                        </div>
                        <div class="column small-12">
                            <div class="ordering-radio change-personal-type">
                                <div class="abc-radio">
                                    <input type="radio" name="radio1" id="radio1" value="option1" data-type="entity" checked>
                                    <label for="radio1">
                                        Юридическое лицо
                                    </label>
                                </div>
                                <div class="abc-radio">
                                    <input type="radio" name="radio1" id="radio2" value="option2"
                                           data-type="individual">
                                    <label for="radio2">
                                        физическое лицо
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="column small-12 large-6">
                            <div class="b-input">
                                <div>
                                    <span class="b-input__title">Имя контактного лица</span>
                                    <label>
                                        <input placeholder="ФИО контактного лица" type="text" required>
                                    </label>
                                </div>
                            </div>
                            <div class="b-input">
                                <div>
                                    <span class="b-input__title">Фамилия контактного лица</span>
                                    <label>
                                        <input placeholder="ФИО контактного лица" type="text" required>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="column small-12 large-6">
                            <div class="b-input -warning">
                                <div>
                                    <span class="b-input__title">Телефон</span>
                                    <label>
                                        <input value="мер" placeholder="Телефон" type="text">
                                    </label>
                                </div>
                            </div>
                            <div class="b-input">
                                <div>
                                    <span class="b-input__title">Эл. почта</span>
                                    <label>
                                        <input placeholder="Введите название организации" type="text">
                                    </label>
                                </div>
                            </div>
                            <div id="entity-fields">
                                <div>
                                    <span class="b-input__title">Прикрепить реквизиты</span>
                                    <div class="b-input flex-wrap">
                                        <input type="file" style="display: none;" id="order-requisites-file">
                                        <button class="btn-reg -b-radius-left" id="order-requisites-btn" type="button">
                                            <svg class="icon icon-upload">
                                                <use xlink:href="#icon-upload"></use>
                                            </svg>
                                            <span>прикрепить</span>
                                        </button>
                                        <button class="btn-reg -b-radius-right" id="write-requisites-open" type="button">
                                            <svg class="icon icon-menu-stroke">
                                                <use xlink:href="#icon-menu-stroke"></use>
                                            </svg>
                                            <span>заполнить</span>
                                        </button>
                                        <span id="requisites-file-name"></span>
                                    </div>
                                </div>
                                <div id="requisites-basket" style="display: none">
                                    <div class="b-input">
                                        <div>
                                            <span class="b-input__title">Наименование организаци </span>
                                            <label>
                                                <input name="company_name" placeholder="Введите название организации" type="text" required>
                                            </label>
                                        </div>
                                    </div>

                                    <div class="b-input">
                                        <div>
                                            <span class="b-input__title">ИНН</span>
                                            <label>
                                                <input placeholder="Введите ИНН" type="text">
                                            </label>
                                        </div>
                                        <div>
                                            <span class="b-input__title">КПП</span>
                                            <label>
                                                <input placeholder="Введите ИНН" type="text">
                                            </label>
                                        </div>
                                    </div>

                                    <div class="b-input">
                                        <div>
                                            <span class="b-input__title">Юридический адрес</span>
                                            <label>
                                                <input placeholder="ВВведите Юридический адрес" type="text">
                                            </label>
                                        </div>
                                    </div>

                                    <div class="b-input">
                                        <div>
                                            <span class="b-input__title">Банк</span>
                                            <label>
                                                <input placeholder="ВВведите Банк" type="text">
                                            </label>
                                        </div>
                                        <div>
                                            <span class="b-input__title">БИК</span>
                                            <label>
                                                <input placeholder="ВВведите БИК " type="text">
                                            </label>
                                        </div>
                                    </div>

                                    <div class="b-input">
                                        <div>
                                            <span class="b-input__title">К/счет </span>
                                            <label>
                                                <input placeholder="ВВведите К/счет" type="text">
                                            </label>
                                        </div>
                                        <div>
                                            <span class="b-input__title">Р/счет</span>
                                            <label>
                                                <input placeholder="ВВведите Р/счет" type="text">
                                            </label>
                                        </div>
                                    </div>

                                    <div class="b-input">
                                        <div>
                                            <span class="b-input__title">Генеральный директор</span>
                                            <label>
                                                <input placeholder="Генеральный директор" type="text">
                                            </label>
                                        </div>
                                    </div>

                                    <div class="b-input">
                                        <div>
                                            <span class="b-input__title">Главный бухгалтер</span>
                                            <label>
                                                <input placeholder="Главный бухгалтер" type="text">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-right">
                                <button type="submit" class="button">отправить</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
</main>
<? include 'partials/footer.php' ?>
<script>
    $(document).foundation();
    $('.count input').on('changequantity', function(e, data) {
        // data.new_value
        // data.old_value
    })
</script>

</body>
</html>

